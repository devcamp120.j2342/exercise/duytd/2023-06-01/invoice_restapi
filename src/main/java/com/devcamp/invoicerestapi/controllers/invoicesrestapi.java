package com.devcamp.invoicerestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.invoicerestapi.models.Invoices;
@RestController
@RequestMapping("/")
public class invoicesrestapi {
    @GetMapping("/invoices")
    public ArrayList<Invoices> getInvoices(){

        

        Invoices invoice1 = new Invoices("1", "Tivi", 100, 14500000);
        Invoices invoice2 = new Invoices("2", "SmartPhone", 200, 10500000);
        Invoices invoice3 = new Invoices("3", "SmartWatch", 100, 6500000);

        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);

        ArrayList<Invoices> ArrListInvoices = new ArrayList<Invoices>();
        ArrListInvoices.add(invoice1);
        ArrListInvoices.add(invoice2);
        ArrListInvoices.add(invoice3);

        return ArrListInvoices;
    }
}
